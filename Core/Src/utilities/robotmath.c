/*
 * robotmath.h
 *
 *  Created on: Mar 13, 2020
 *      Author: Glebk
 */

float mapv(float inmin, float inmax, float outmin, float outmax, float value) {
	float upperin = value - inmin;
	float lowerin = inmax - inmin;

	float lowerout = outmax - outmin;

	return lowerout * upperin / lowerin + outmin;
}

float clamp(float min, float max, float value){

	if (value > max){
		return max;
	}
	if (value < min){
		return min;
	}
	return value;

}

