/*
 * motor.c
 *
 *  Created on: Mar 13, 2020
 *      Author: Glebk
 */

#include <inttypes.h>
#include <stdlib.h>
#include <stm32f3xx_hal.h>
#include "receiver.h"
#include "motor.h"
#include "robotmath.h"

motor_t* motor_new(timer_t *timer, channel_t channel){

	motor_t *motor = malloc(sizeof(motor_t));

	motor->timer = timer;
	motor->channel = channel;

	return motor;
}

void motor_set_value(motor_t* motor, motor_value_t value){

	__HAL_TIM_SET_COMPARE(motor->timer, motor->channel, value);

}

axis_t* axis_new(timer_t *l_timer, channel_t l_channel, timer_t *r_timer, channel_t r_channel){

	axis_t *axis = malloc(sizeof(axis_t));

	axis->left = motor_new(l_timer, l_channel);
	axis->right = motor_new(r_timer, r_channel);

	return axis;
}


float _axis_encode_stick(motor_value_t stick_value) {
	return mapv(PWM_STICK_MIN_VALUE, PWM_STICK_MAX_VALUE, STICK_MIN_VALUE, STICK_MAX_VALUE, stick_value);
}

motor_value_t _axis_decode_stick(float stick_value) {
	return (motor_value_t) mapv(STICK_MIN_VALUE, STICK_MAX_VALUE, PWM_STICK_MIN_VALUE, PWM_STICK_MAX_VALUE, stick_value);
}

void _axis_calculate(motor_value_t throttle, motor_value_t turn, uint8_t divisor,
					 motor_value_t* l_motor, motor_value_t* r_motor)
{
	float th = _axis_encode_stick(throttle),
	      tu = _axis_encode_stick(turn);

	float lm = th + tu / divisor,
		  rm = th - tu / divisor;

	*l_motor = _axis_decode_stick(lm);
	*r_motor = _axis_decode_stick(rm);
}

void axis_move(axis_t* axis, receiver_t* reciever){

	motor_value_t l_motor, r_motor;
	motor_value_t throttle, turn;
	uint8_t is_fail_safe;
	uint8_t divisor = 4;


	// FIX
	switch (2048){//receiver_get_power_switch_value(reciever)){
	// TODO also reset weapon in future like we're resetting the motors
	case PWM_SWITCH_MODE_ZERO:
		motor_set_value(axis->left, MOTOR_OFF_VALUE);
		motor_set_value(axis->right, MOTOR_OFF_VALUE);
		break;

	case PWM_SWITCH_MODE_TWO:
		divisor = 3;

	case PWM_SWITCH_MODE_ONE:
		throttle = receiver_get_throttle(reciever, &is_fail_safe);
		//mapv the value before setting

		if (is_fail_safe == 1) {}

		turn = receiver_get_turn(reciever, &is_fail_safe);

		if (is_fail_safe == 1) {}

		_axis_calculate(throttle, turn, divisor, &l_motor, &r_motor);
		motor_set_value(axis->left, l_motor);
		motor_set_value(axis->right, r_motor);
		break;

	}
}

void axis_delete(axis_t* axis) {
	motor_set_value(axis->left, MOTOR_OFF_VALUE);
	motor_set_value(axis->right, MOTOR_OFF_VALUE);

	free(axis->left);
	free(axis->right);
	free(axis);
}
