#include <inttypes.h>
#include <stm32f3xx_hal.h>
#include "receiver.h"

#pragma once

typedef uint8_t channel_t;
typedef uint16_t motor_value_t;
typedef TIM_HandleTypeDef timer_t;

#define MOTOR_OFF_VALUE 1536

typedef struct Motor{
	timer_t *timer;
	channel_t channel;
} motor_t;

motor_t* motor_new(timer_t *timer, channel_t channel);

void motor_set_value(motor_t* motor, motor_value_t value);




typedef struct Axis{

	motor_t *left;
	motor_t *right;

} axis_t;

axis_t* axis_new(timer_t *l_timer, channel_t l_channel, timer_t *r_timer, channel_t r_channel);

void axis_move(axis_t*, receiver_t*);

void axis_delete(axis_t*);
