/*
 * receiver.c
 *
 *  Created on: Mar 12, 2020
 *      Author: Glebk
 */


#include <stdlib.h>
#include <inttypes.h>
#include <stm32f3xx_hal.h>
#include "receiver.h"

switch_t* switch_new(TIM_HandleTypeDef *timer, uint8_t channel) {
	switch_t *sw = malloc(sizeof(switch_t));
	sw->timer = timer;
	sw->channel = channel;

	return sw;
}

uint16_t switch_read(const switch_t* sw) {
	return 1536;//(uint16_t) HAL_TIM_ReadCapturedValue(sw->timer, sw->channel);
}

uint8_t switch_encode(float value){
	return mapv(PWM_SWITCH_MODE_ZERO, PWM_SWITCH_MODE_TWO, SWITCH_MODE_ZERO, SWITCH_MODE_TWO, value);
}

uint8_t switch_decode(float value){
	return mapv(SWITCH_MODE_ZERO, SWITCH_MODE_TWO, PWM_SWITCH_MODE_ZERO, PWM_SWITCH_MODE_TWO, value);
}


rcinput_t* input_new(TIM_HandleTypeDef *timer, uint8_t channel) {
	rcinput_t *st = malloc(sizeof(rcinput_t));
	st->timer = timer;
	st->channel = channel;
	st->value = PWM_STICK_MID_VALUE;
	st->is_fail_safe = 0;

	return st;
}

uint16_t input_read(rcinput_t* st) {
	uint16_t value = (uint16_t) HAL_TIM_ReadCapturedValue(st->timer, st->channel);

	st->value = value; //TODO handle fail safe scenario PWM_STICK_MID_VALUE;
	st->is_fail_safe = 0;//1

	return st->value;
}

receiver_t* receiver_new(TIM_HandleTypeDef *on_off_timer, channel_t on_off_channel,
						 TIM_HandleTypeDef *spin_direction_timer, channel_t spin_direction_channel,
						 TIM_HandleTypeDef *throttle_timer, channel_t throttle_channel,
						 TIM_HandleTypeDef *turn_timer, channel_t turn_channel)
{
	receiver_t *recv = malloc(sizeof(receiver_t));
	recv->on_off_switch = input_new(on_off_timer, on_off_channel);
	recv->spin_direction = input_new(spin_direction_timer, spin_direction_channel);
	recv->throttle = input_new(throttle_timer, throttle_channel);
	recv->turn = input_new(turn_timer, turn_channel);

	return recv;
}

uint16_t receiver_get_power_switch_value(receiver_t* recv) {

	return input_read(recv->on_off_switch);
}

uint16_t receiver_get_spin_direction(receiver_t* recv) {

	return input_read(recv->spin_direction);
}

uint16_t _receiver_read_stick(rcinput_t* st, uint8_t* is_fail_safe) {
	uint16_t value = input_read(st);

	if (NULL != is_fail_safe) {
		*is_fail_safe = st->is_fail_safe;
	}

	return value;
}

uint16_t receiver_get_throttle(receiver_t* recv, uint8_t* is_fail_safe) {
	return _receiver_read_stick(recv->throttle, is_fail_safe);
}

uint16_t receiver_get_turn(receiver_t* recv, uint8_t* is_fail_safe) {
	return _receiver_read_stick(recv->turn, is_fail_safe);
}

void receiver_delete(receiver_t* recv) {
	free(recv->on_off_switch);
	free(recv->spin_direction);
	free(recv->throttle);
	free(recv->turn);
	free(recv);
}
