#include <inttypes.h>
#include <stm32f3xx_hal.h>
#include "robotmath.h"

#pragma once

#define PWM_SWITCH_MODE_ZERO 1024
#define PWM_SWITCH_MODE_ONE 1536
#define PWM_SWITCH_MODE_TWO 2048

#define SWITCH_MODE_ZERO 0
#define SWITCH_MODE_ONE 1
#define SWITCH_MODE_TWO 2

#define PWM_STICK_MAX_VALUE 2048
#define PWM_STICK_MID_VALUE 1536
#define PWM_STICK_MIN_VALUE 1024

#define STICK_MAX_VALUE 100
#define STICK_MID_VALUE 0
#define STICK_MIN_VALUE -100

typedef uint8_t channel_t;

typedef struct Switch {
	TIM_HandleTypeDef *timer;
	uint8_t channel;
} switch_t;

switch_t* switch_new(TIM_HandleTypeDef *timer, uint8_t channel);

uint16_t switch_read(const switch_t*);

uint8_t switch_encode(float value);

uint8_t switch_decode(float value);


typedef struct RCInput {
	TIM_HandleTypeDef *timer;
	uint8_t channel;
	uint16_t value;
	uint8_t is_fail_safe;
} rcinput_t;

rcinput_t* input_new(TIM_HandleTypeDef *timer, channel_t channel);

uint16_t input_read(rcinput_t*);

typedef struct Receiver {
	rcinput_t *on_off_switch;
	rcinput_t *spin_direction;
	rcinput_t *throttle;
	rcinput_t *turn;
} receiver_t;
//uoguyfkyg,j
receiver_t* receiver_new(TIM_HandleTypeDef *on_off_timer, channel_t on_off_channel,
						 TIM_HandleTypeDef *spin_direction_timer, channel_t spin_direction_channel,
						 TIM_HandleTypeDef *throttle_timer, channel_t throttle_channel,
						 TIM_HandleTypeDef *turn_timer, channel_t turn_channel);

uint16_t receiver_get_power_switch_value(receiver_t*);
uint16_t receiver_get_spin_direction(receiver_t*);
uint16_t receiver_get_throttle(receiver_t*, uint8_t*);
uint16_t receiver_get_turn(receiver_t*, uint8_t*);
void receiver_delete(receiver_t*);
