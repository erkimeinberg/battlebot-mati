/*
 * robotmath.h
 *
 *  Created on: Mar 31, 2020
 *      Author: Glebk
 */

#ifndef SRC_UTILITIES_ROBOTMATH_H_
#define SRC_UTILITIES_ROBOTMATH_H_

float mapv(float inmin, float inmax, float outmin, float outmax, float value);
float clamp(float min, float max, float value);

#endif /* SRC_UTILITIES_ROBOTMATH_H_ */
